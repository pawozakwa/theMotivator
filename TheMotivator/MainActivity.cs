﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;

namespace TheMotivator
{
    [Activity(Label = "TheMotivator", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            InitFlagsValues();

            // Set our view from the "main" layout resource
            // SetContentView (Resource.Layout.Main);
        }

        private void InitFlagsValues()
        {
            this.Window.AddFlags(WindowManagerFlags.Fullscreen);
            RequestWindowFeature(WindowFeatures.NoTitle);
        }
    }
}

