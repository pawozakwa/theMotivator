﻿using RestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RestApi.ModelsOfTransactions
{
    [Serializable]
    public class SerializableSentence
    {
        public int Id { get; }
        public string Title { get; }
        public string Text { get; }

        public List<string> Tags { get; }
        public List<int> TagsIds { get; }

        public string Color1 { get; }
        public string Color2 { get; }
        public string Background { get; }

        public SerializableSentence(Sentence sentence, SentenceTranslation translation, List<SentenceTag>tagsList, List<SentenceTagTranslation> tagsTranslationsList)
        {
            Id = sentence.SentenceId;
            Title = translation.Title;
            Text = translation.Text;
            Color1 = sentence.Color1;
            Color2 = sentence.Color2;
            Background = sentence.Background;

            Tags = new List<string>();
            TagsIds = new List<int>();
            
            foreach (var tag in tagsList)
            {
                TagsIds.Add(tag.TagId);
            }

            foreach (var tagTranslation in tagsTranslationsList)
            {
                Tags.Add(tagTranslation.Name);
            }
        }
    }
}