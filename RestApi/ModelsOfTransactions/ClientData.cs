﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestApi.Helpers;
using RestApi.Models;

namespace RestApi.ModelsOfTransactions
{
    [Serializable]
    public class ClientData
    {
        public int UserId { get; }
        public string DeviceToken { get; }
        public string OAuth_provider { get; }
        public string OAuth_uid { get;  }
        public string Username { get; }
        public int LanguageId { get; }

        public DateTime LastVisitTime { get; }

        public List<SerializableSentence> OwnedSentences { get; }
        public SerializableSentence ActualSentence { get; set; }
        

        public ClientData(User u, MotivatorModelContainer context)
        {
            UserId = u.UserId;
            DeviceToken = u.DeviceToken;
            OAuth_provider = u.OAuth_provider;
            OAuth_uid = u.OAuth_uid;
            Username = u.Username;
            LanguageId = u.Language.LanguageId;
            LastVisitTime = u.LastVisitTime;

            OwnedSentences = u.GetAllOwnedSentences(context);
            ActualSentence = u.GetActualSentece(context);
        }
    }
}