
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/12/2017 20:18:06
-- Generated from EDMX file: F:\C# Projects\TheMotivator\RestApi\Models\MotivatorModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [MotivatorDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UserSentence_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserSentence] DROP CONSTRAINT [FK_UserSentence_User];
GO
IF OBJECT_ID(N'[dbo].[FK_UserSentence_Sentence]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserSentence] DROP CONSTRAINT [FK_UserSentence_Sentence];
GO
IF OBJECT_ID(N'[dbo].[FK_ActualSentence]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_ActualSentence];
GO
IF OBJECT_ID(N'[dbo].[FK_LanguageSentenceTranslation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SentenceTranslations] DROP CONSTRAINT [FK_LanguageSentenceTranslation];
GO
IF OBJECT_ID(N'[dbo].[FK_SentenceTagTranslationLanguage]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SentenceTagTranslations] DROP CONSTRAINT [FK_SentenceTagTranslationLanguage];
GO
IF OBJECT_ID(N'[dbo].[FK_SentenceTagTranslationSentenceTag]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SentenceTagTranslations] DROP CONSTRAINT [FK_SentenceTagTranslationSentenceTag];
GO
IF OBJECT_ID(N'[dbo].[FK_SentenceTagSentence_SentenceTag]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SentenceToTag] DROP CONSTRAINT [FK_SentenceTagSentence_SentenceTag];
GO
IF OBJECT_ID(N'[dbo].[FK_SentenceTagSentence_Sentence]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SentenceToTag] DROP CONSTRAINT [FK_SentenceTagSentence_Sentence];
GO
IF OBJECT_ID(N'[dbo].[FK_SentenceSentenceTranslation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SentenceTranslations] DROP CONSTRAINT [FK_SentenceSentenceTranslation];
GO
IF OBJECT_ID(N'[dbo].[FK_LanguageUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_LanguageUser];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Sentences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sentences];
GO
IF OBJECT_ID(N'[dbo].[SentenceTranslations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SentenceTranslations];
GO
IF OBJECT_ID(N'[dbo].[Languages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Languages];
GO
IF OBJECT_ID(N'[dbo].[SentenceTags]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SentenceTags];
GO
IF OBJECT_ID(N'[dbo].[SentenceTagTranslations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SentenceTagTranslations];
GO
IF OBJECT_ID(N'[dbo].[UserSentence]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserSentence];
GO
IF OBJECT_ID(N'[dbo].[SentenceToTag]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SentenceToTag];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserId] int IDENTITY(1,1) NOT NULL,
    [DeviceToken] nvarchar(max)  NOT NULL,
    [OAuth_provider] nvarchar(max)  NULL,
    [OAuth_uid] nvarchar(max)  NULL,
    [Username] nvarchar(max)  NOT NULL,
    [LastVisitTime] datetime  NOT NULL,
    [ActualSentence_SentenceId] int  NULL,
    [Language_LanguageId] int  NOT NULL
);
GO

-- Creating table 'Sentences'
CREATE TABLE [dbo].[Sentences] (
    [SentenceId] int IDENTITY(1,1) NOT NULL,
    [Color1] nvarchar(7)  NULL,
    [Color2] nvarchar(7)  NOT NULL,
    [Background] nvarchar(max)  NULL
);
GO

-- Creating table 'SentenceTranslations'
CREATE TABLE [dbo].[SentenceTranslations] (
    [TranslationId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NOT NULL,
    [Text] nvarchar(max)  NOT NULL,
    [SentenceSentenceId] int  NOT NULL,
    [Language_LanguageId] int  NOT NULL
);
GO

-- Creating table 'Languages'
CREATE TABLE [dbo].[Languages] (
    [LanguageId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'SentenceTags'
CREATE TABLE [dbo].[SentenceTags] (
    [TagId] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'SentenceTagTranslations'
CREATE TABLE [dbo].[SentenceTagTranslations] (
    [TagTransaltionId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Language_LanguageId] int  NOT NULL,
    [SentenceTag_TagId] int  NOT NULL
);
GO

-- Creating table 'UserSentence'
CREATE TABLE [dbo].[UserSentence] (
    [UserSentence_Sentence_UserId] int  NOT NULL,
    [OwnedSentences_SentenceId] int  NOT NULL
);
GO

-- Creating table 'SentenceToTag'
CREATE TABLE [dbo].[SentenceToTag] (
    [SentenceTags_TagId] int  NOT NULL,
    [TagedSentences_SentenceId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [UserId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [SentenceId] in table 'Sentences'
ALTER TABLE [dbo].[Sentences]
ADD CONSTRAINT [PK_Sentences]
    PRIMARY KEY CLUSTERED ([SentenceId] ASC);
GO

-- Creating primary key on [TranslationId] in table 'SentenceTranslations'
ALTER TABLE [dbo].[SentenceTranslations]
ADD CONSTRAINT [PK_SentenceTranslations]
    PRIMARY KEY CLUSTERED ([TranslationId] ASC);
GO

-- Creating primary key on [LanguageId] in table 'Languages'
ALTER TABLE [dbo].[Languages]
ADD CONSTRAINT [PK_Languages]
    PRIMARY KEY CLUSTERED ([LanguageId] ASC);
GO

-- Creating primary key on [TagId] in table 'SentenceTags'
ALTER TABLE [dbo].[SentenceTags]
ADD CONSTRAINT [PK_SentenceTags]
    PRIMARY KEY CLUSTERED ([TagId] ASC);
GO

-- Creating primary key on [TagTransaltionId] in table 'SentenceTagTranslations'
ALTER TABLE [dbo].[SentenceTagTranslations]
ADD CONSTRAINT [PK_SentenceTagTranslations]
    PRIMARY KEY CLUSTERED ([TagTransaltionId] ASC);
GO

-- Creating primary key on [UserSentence_Sentence_UserId], [OwnedSentences_SentenceId] in table 'UserSentence'
ALTER TABLE [dbo].[UserSentence]
ADD CONSTRAINT [PK_UserSentence]
    PRIMARY KEY CLUSTERED ([UserSentence_Sentence_UserId], [OwnedSentences_SentenceId] ASC);
GO

-- Creating primary key on [SentenceTags_TagId], [TagedSentences_SentenceId] in table 'SentenceToTag'
ALTER TABLE [dbo].[SentenceToTag]
ADD CONSTRAINT [PK_SentenceToTag]
    PRIMARY KEY CLUSTERED ([SentenceTags_TagId], [TagedSentences_SentenceId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserSentence_Sentence_UserId] in table 'UserSentence'
ALTER TABLE [dbo].[UserSentence]
ADD CONSTRAINT [FK_UserSentence_User]
    FOREIGN KEY ([UserSentence_Sentence_UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [OwnedSentences_SentenceId] in table 'UserSentence'
ALTER TABLE [dbo].[UserSentence]
ADD CONSTRAINT [FK_UserSentence_Sentence]
    FOREIGN KEY ([OwnedSentences_SentenceId])
    REFERENCES [dbo].[Sentences]
        ([SentenceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserSentence_Sentence'
CREATE INDEX [IX_FK_UserSentence_Sentence]
ON [dbo].[UserSentence]
    ([OwnedSentences_SentenceId]);
GO

-- Creating foreign key on [ActualSentence_SentenceId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_ActualSentence]
    FOREIGN KEY ([ActualSentence_SentenceId])
    REFERENCES [dbo].[Sentences]
        ([SentenceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ActualSentence'
CREATE INDEX [IX_FK_ActualSentence]
ON [dbo].[Users]
    ([ActualSentence_SentenceId]);
GO

-- Creating foreign key on [Language_LanguageId] in table 'SentenceTranslations'
ALTER TABLE [dbo].[SentenceTranslations]
ADD CONSTRAINT [FK_LanguageSentenceTranslation]
    FOREIGN KEY ([Language_LanguageId])
    REFERENCES [dbo].[Languages]
        ([LanguageId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LanguageSentenceTranslation'
CREATE INDEX [IX_FK_LanguageSentenceTranslation]
ON [dbo].[SentenceTranslations]
    ([Language_LanguageId]);
GO

-- Creating foreign key on [Language_LanguageId] in table 'SentenceTagTranslations'
ALTER TABLE [dbo].[SentenceTagTranslations]
ADD CONSTRAINT [FK_SentenceTagTranslationLanguage]
    FOREIGN KEY ([Language_LanguageId])
    REFERENCES [dbo].[Languages]
        ([LanguageId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SentenceTagTranslationLanguage'
CREATE INDEX [IX_FK_SentenceTagTranslationLanguage]
ON [dbo].[SentenceTagTranslations]
    ([Language_LanguageId]);
GO

-- Creating foreign key on [SentenceTag_TagId] in table 'SentenceTagTranslations'
ALTER TABLE [dbo].[SentenceTagTranslations]
ADD CONSTRAINT [FK_SentenceTagTranslationSentenceTag]
    FOREIGN KEY ([SentenceTag_TagId])
    REFERENCES [dbo].[SentenceTags]
        ([TagId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SentenceTagTranslationSentenceTag'
CREATE INDEX [IX_FK_SentenceTagTranslationSentenceTag]
ON [dbo].[SentenceTagTranslations]
    ([SentenceTag_TagId]);
GO

-- Creating foreign key on [SentenceTags_TagId] in table 'SentenceToTag'
ALTER TABLE [dbo].[SentenceToTag]
ADD CONSTRAINT [FK_SentenceTagSentence_SentenceTag]
    FOREIGN KEY ([SentenceTags_TagId])
    REFERENCES [dbo].[SentenceTags]
        ([TagId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TagedSentences_SentenceId] in table 'SentenceToTag'
ALTER TABLE [dbo].[SentenceToTag]
ADD CONSTRAINT [FK_SentenceTagSentence_Sentence]
    FOREIGN KEY ([TagedSentences_SentenceId])
    REFERENCES [dbo].[Sentences]
        ([SentenceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SentenceTagSentence_Sentence'
CREATE INDEX [IX_FK_SentenceTagSentence_Sentence]
ON [dbo].[SentenceToTag]
    ([TagedSentences_SentenceId]);
GO

-- Creating foreign key on [SentenceSentenceId] in table 'SentenceTranslations'
ALTER TABLE [dbo].[SentenceTranslations]
ADD CONSTRAINT [FK_SentenceSentenceTranslation]
    FOREIGN KEY ([SentenceSentenceId])
    REFERENCES [dbo].[Sentences]
        ([SentenceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SentenceSentenceTranslation'
CREATE INDEX [IX_FK_SentenceSentenceTranslation]
ON [dbo].[SentenceTranslations]
    ([SentenceSentenceId]);
GO

-- Creating foreign key on [Language_LanguageId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_LanguageUser]
    FOREIGN KEY ([Language_LanguageId])
    REFERENCES [dbo].[Languages]
        ([LanguageId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LanguageUser'
CREATE INDEX [IX_FK_LanguageUser]
ON [dbo].[Users]
    ([Language_LanguageId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------