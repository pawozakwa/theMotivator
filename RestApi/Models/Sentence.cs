//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RestApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sentence
    {
        public Sentence()
        {
            this.Color1 = "#EFF8FB";
            this.Color2 = "#CEECF5";
            this.SentenceTags = new HashSet<SentenceTag>();
            this.SentenceTranslations = new HashSet<SentenceTranslation>();
        }
    
        public int SentenceId { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }
        public string Background { get; set; }
    
        public virtual ICollection<SentenceTag> SentenceTags { get; set; }
        public virtual ICollection<SentenceTranslation> SentenceTranslations { get; set; }
    }
}
