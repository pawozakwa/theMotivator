using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using NUnit.Framework;
using RestApi.Models;

namespace RestApi.Migrations
{
    [TestFixture]
    class MigrationThroughTest
    {
        [Test]
        public void SeedDatebase()
        {
            var english = new Language { Name = "english" };
            var polish = new Language { Name = "polish" };
            var langs = new List<Language>()
            {
                english,
                polish
            };

            var tags = new List<SentenceTag>()
            {
                new SentenceTag()
            };

            var tagTranslations = new List<SentenceTagTranslation>()
            {
                new SentenceTagTranslation()
                {
                    Name = "#testTag",
                    Language = langs.Find(l => l.Name == "english"),
                    SentenceTag = tags[0]
                }
            };

            var sentences = new List<Sentence>()
            {
                new Sentence(),
                new Sentence(),
                new Sentence(),
                new Sentence()
            };
            foreach (var s in sentences)
                s.SentenceTags.Add(tags[0]);

            var sentenceTranslations = new List<SentenceTranslation>()
            {
                new SentenceTranslation()
                {
                    Language = english,
                    Title = "Desiderata",
                    Text = @"Go placidly amid the noise and haste,\r\n and remember what peace there may be in silence.\r\n As far as possible without surrender\r\n be on good terms with all persons.\r\n Speak your truth quietly and clearly;\r\n                    and listen to others,\r\n even the dull and the ignorant;\r\n they too have their story.\r\n Avoid loud and aggressive persons,\r\n they are vexations to the spirit.\r\n If you compare yourself with others,\r\n you may become vain and bitter;\r\n for always there will be greater and lesser persons than yourself.\r\n Enjoy your achievements as well as your plans.\r\n Keep interested in your own career, however humble;\r\n it is a real possession in the changing fortunes of time.\r\n Exercise caution in your business affairs;\r\n for the world is full of trickery.\r\n But let this not blind you to what virtue there is;\r\n many persons strive for high ideals;\r\n and everywhere life is full of heroism.\r\n Be yourself.\r\n Especially, do not feign affection.\r\n Neither be cynical about love;\r\n for in the face of all aridity and disenchantment\r\n it is as perennial as the grass.\r\n Take kindly the counsel of the years,\r\n gracefully surrendering the things of youth.\r\n Nurture strength of spirit to shield you in sudden misfortune.\r\n But do not distress yourself with dark imaginings.\r\n Many fears are born of fatigue and loneliness.\r\n Beyond a wholesome discipline,\r\n be gentle with yourself.\r\n You are a child of the universe,\r\n no less than the trees and the stars;\r\n you have a right to be here.\r\n And whether or not it is clear to you,\r\n no doubt the universe is unfolding as it should.\r\n Therefore be at peace with God,\r\n whatever you conceive Him to be,\r\n and whatever your labors and aspirations,\r\n in the noisy confusion of life keep peace with your soul. \r\n With all its sham, drudgery, and broken dreams,\r\n it is still a beautiful world.\r\n Be cheerful.\r\n Strive to be happy. \r\n Max Ehrmann",
                    Sentence = sentences[0]
                },
                new SentenceTranslation()
                {
                    Language = polish,
                    Title = "Desiderata",
                    Text = @"Krocz spokojnie w�r�d zgie�ku i po�piechu - pami�taj jaki pok�j mo�e by� w ciszy. Tak dalece jak to mo�liwe nie wyrzekaj�c si� siebie, b�d� w dobrych stosunkach z innymi lud�mi. Prawd� sw� g�o� spokojnie i jasno, s�uchaj te� tego co m�wi� inni, nawet g�upcy i ignoranci, oni te� maj� swoj� opowie��. Je�li por�wnujesz si� z innymi mo�esz sta� si� pr�ny lub zgorzknia�y, albowiem zawsze b�d� lepsi i gorsi od ciebie. \r\n\r\n     Ciesz si� zar�wno swymi osi�gni�ciami jak i planami. Wykonuj z sercem sw� prac�, jakakolwiek by by�a skromna. Jest ona trwa�� warto�ci� w zmiennych kolejach losu. Zachowaj ostro�no�� w swych przedsi�wzi�ciach - �wiat bowiem pe�en jest oszustwa. Lecz niech ci to nie przes�ania prawdziwej cnoty, wielu ludzi d��y do wznios�ych idea��w i wsz�dzie �ycie pe�ne jest heroizmu.\r\n\r\n     B�d� sob�, a zw�aszcza nie zwalczaj uczu�: nie b�d� cyniczny wobec mi�o�ci, albowiem w obliczu wszelkiej osch�o�ci i rozczarowa� jest ona wieczna jak trawa. Przyjmuj pogodnie to co lata nios�, bez goryczy wyrzekaj�c si� przymiot�w m�odo�ci. Rozwijaj si�� ducha by w nag�ym nieszcz�ciu mog�a by� tarcz� dla ciebie. Lecz nie dr�cz si� tworami wyobra�ni. Wiele obaw rodzi si� ze znu�enia i samotno�ci.\r\n\r\n     Jeste� dzieckiem wszech�wiata, nie mniej ni� gwiazdy i drzewa, masz prawo by� tutaj i czy jest to dla ciebie jasne czy nie, nie w�tp, �e wszech�wiat jest taki jaki by� powinien.\r\n\r\n     Tak wi�c b�d� w pokoju z Bogiem, cokolwiek my�lisz i czymkolwiek si� zajmujesz i jakiekolwiek s� twe pragnienia: w zgie�ku ulicznym, zam�cie �ycia, zachowaj pok�j ze sw� dusz�. Z ca�ym swym zak�amaniem, znojem i rozwianymi marzeniami ci�gle jeszcze ten �wiat jest pi�kny. B�d� uwa�ny, staraj si� by� szcz�liwy.\r\n\r\nMax Ehrmann",
                    Sentence = sentences[0]
                },

                new SentenceTranslation()
                {
                    Language = english,
                    Title = "Einstein bike",
                    Text = @"Life is like riding a bicycle. To keep your balance, you must keep moving.\r\n Albert Einstein",
                    Sentence = sentences[1]
                },
                new SentenceTranslation()
                {
                    Language = polish,
                    Title = "Rower einsteina",
                    Text = @"�ycie jest jak jazda na rowerze.\r\n �eby utrzyma� r�wnowag� musisz si� porusza� naprz�d.\r\n Albert Einstein",
                    Sentence = sentences[1]
                },

                new SentenceTranslation()
                {
                    Language = polish,
                    Title = "Dwa wilki",
                    Text = @"Pewien stary Indianin Cherokee naucza� swoje wnuki. Powiedzia� im tak:\r\n� Wewn�trz mnie odbywa si� walka. To straszna walka.\r\n\r\n\r\nWalcz� dwa wilki:\r\n\r\njeden reprezentuje strach, z�o��, zazdro��, smutek, �al, chciwo��, arogancj�, u�alanie si� nad sob�, poczucie winy, uraz�, poczucie ni�szo�ci, k�amstwa, fa�szyw� dum� i poczucie wy�szo�ci.\r\n\r\nDrugi to rado��, zadowolenie, zgoda, pok�j, mi�o��, nadzieja, akceptacja, ch�� zrozumienia, hojno��, prawda, �yczliwo��, wsp�czucie i wiara.\r\n\r\nTaka sama walka odbywa si� wewn�trz was i ka�dej innej osoby.\r\n\r\nDzieci my�la�y o tym przez chwil�, po czym jedno z nich zapyta�o:\r\n� Dziadku,  a kt�ry wilk wygra?\r\n� Ten, kt�rego nakarmisz � odpowiedzia� stary Indianin.",
                    Sentence = sentences[2]
                },
                new SentenceTranslation()
                {
                    Language = english,
                    Title = "Two wolves",
                    Text = @"An old Cherokee is teaching his grandson about life. \r\n-It is a terrible fight and it is between two wolves. One is evil � he is anger, envy, sorrow, regret, greed, arrogance, self-pity, guilt, resentment, inferiority, lies, false pride, superiority, and ego.\r\n- He continued, \r\n-The other is good � he is joy, peace, love, hope, serenity, humility, kindness, benevolence, empathy, generosity, truth, compassion, and faith. The same fight is going on inside you � and inside every other person, too. The grandson thought about it for a minute and then asked his grandfather, \r\n-Which wolf will win? \r\n-The old Cherokee simply replied, \r\n-The one you feed.",
                    Sentence = sentences[2]
                },

                new SentenceTranslation()
                {
                    Language = polish,
                    Title = "Fortuna",
                    Text = @"Fortes fortuna adiuvat \r\n Szcz�cie sprzyja �mia�ym",
                    Sentence = sentences[3]
                },
                new SentenceTranslation()
                {
                    Language = english,
                    Title = "Fortuna",
                    Text = @"Fortes fortuna adiuvat \r\n Fortune favors the bold",
                    Sentence = sentences[3]
                }
            };
            using (var context = new MotivatorModelContainer())
            {

                var testUser = new User()
                {
                    DeviceToken = "1",
                    Username = "TestUser",
                    Language = context.Languages.FirstOrDefault(),
                    OwnedSentences = new List<Sentence>(),
                    LastVisitTime = DateTime.Now
                };
                testUser.OwnedSentences.Add(sentences[3]);
                testUser.OwnedSentences.Add(sentences[2]);


                foreach (var lang in langs)
                {
                    context.Languages.AddOrUpdate(lang);
                }
                foreach (var sentence in sentences)
                {
                    context.Sentences.AddOrUpdate(sentence);
                }
                foreach (var translation in sentenceTranslations)
                {
                    context.SentenceTranslations.AddOrUpdate(translation);
                }
                foreach (var tag in tags)
                {
                    context.SentenceTags.AddOrUpdate(tag);
                }
                foreach (var tagTranslation in tagTranslations)
                {
                    context.SentenceTagTranslations.AddOrUpdate(tagTranslation);
                }
                context.Users.AddOrUpdate(testUser);

                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    var errors = "";
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        errors += "- Property: " + eve.Entry.Entity.GetType().Name + ", Error: " + eve.Entry.State + Environment.NewLine;
                        errors = eve.ValidationErrors.Aggregate(errors, (current, ve) => current + ("- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + Environment.NewLine));
                        Console.WriteLine(errors);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            Console.WriteLine("Initiation done");
        }
    }
}
