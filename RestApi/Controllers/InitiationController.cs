﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.UI.WebControls;
using RestApi.Models;

namespace RestApi.Controllers
{
    [RoutePrefix("api")]
    public class InitiationController : ApiController
    {
        [Route("init")]
        public string Get()
        {



            var english = new Language { Name = "english" };
            var polish = new Language { Name = "polish" };
            var langs = new List<Language>()
            {
                english,
                polish
            };

            var tags = new List<SentenceTag>()
            {
                new SentenceTag()
            };

            var tagTranslations = new List<SentenceTagTranslation>()
            {
                new SentenceTagTranslation()
                {
                    Name = "#testTag",
                    Language = english,
                    SentenceTag = tags[0]
                }
            };

            var sentences = new List<Sentence>()
            {
                new Sentence(),
                new Sentence(),
                new Sentence(),
                new Sentence()
            };
            foreach (var s in sentences)
                s.SentenceTags.Add(tags[0]);

            var sentenceTranslations = new List<SentenceTranslation>()
            {
                new SentenceTranslation()
                {
                    Language = english,
                    Title = "Desiderata",
                    Text = @"Go placidly amid the noise and haste,\r\n and remember what peace there may be in silence.\r\n As far as possible without surrender\r\n be on good terms with all persons.\r\n Speak your truth quietly and clearly;\r\n                    and listen to others,\r\n even the dull and the ignorant;\r\n they too have their story.\r\n Avoid loud and aggressive persons,\r\n they are vexations to the spirit.\r\n If you compare yourself with others,\r\n you may become vain and bitter;\r\n for always there will be greater and lesser persons than yourself.\r\n Enjoy your achievements as well as your plans.\r\n Keep interested in your own career, however humble;\r\n it is a real possession in the changing fortunes of time.\r\n Exercise caution in your business affairs;\r\n for the world is full of trickery.\r\n But let this not blind you to what virtue there is;\r\n many persons strive for high ideals;\r\n and everywhere life is full of heroism.\r\n Be yourself.\r\n Especially, do not feign affection.\r\n Neither be cynical about love;\r\n for in the face of all aridity and disenchantment\r\n it is as perennial as the grass.\r\n Take kindly the counsel of the years,\r\n gracefully surrendering the things of youth.\r\n Nurture strength of spirit to shield you in sudden misfortune.\r\n But do not distress yourself with dark imaginings.\r\n Many fears are born of fatigue and loneliness.\r\n Beyond a wholesome discipline,\r\n be gentle with yourself.\r\n You are a child of the universe,\r\n no less than the trees and the stars;\r\n you have a right to be here.\r\n And whether or not it is clear to you,\r\n no doubt the universe is unfolding as it should.\r\n Therefore be at peace with God,\r\n whatever you conceive Him to be,\r\n and whatever your labors and aspirations,\r\n in the noisy confusion of life keep peace with your soul. \r\n With all its sham, drudgery, and broken dreams,\r\n it is still a beautiful world.\r\n Be cheerful.\r\n Strive to be happy. \r\n Max Ehrmann",
                    Sentence = sentences[0]
                },
                new SentenceTranslation()
                {
                    Language = polish,
                    Title = "Desiderata",
                    Text = @"Krocz spokojnie wśród zgiełku i pośpiechu - pamiętaj jaki pokój może być w ciszy. Tak dalece jak to możliwe nie wyrzekając się siebie, bądź w dobrych stosunkach z innymi ludźmi. Prawdę swą głoś spokojnie i jasno, słuchaj też tego co mówią inni, nawet głupcy i ignoranci, oni też mają swoją opowieść. Jeśli porównujesz się z innymi możesz stać się próżny lub zgorzkniały, albowiem zawsze będą lepsi i gorsi od ciebie. \r\n\r\n     Ciesz się zarówno swymi osiągnięciami jak i planami. Wykonuj z sercem swą pracę, jakakolwiek by była skromna. Jest ona trwałą wartością w zmiennych kolejach losu. Zachowaj ostrożność w swych przedsięwzięciach - świat bowiem pełen jest oszustwa. Lecz niech ci to nie przesłania prawdziwej cnoty, wielu ludzi dąży do wzniosłych ideałów i wszędzie życie pełne jest heroizmu.\r\n\r\n     Bądź sobą, a zwłaszcza nie zwalczaj uczuć: nie bądź cyniczny wobec miłości, albowiem w obliczu wszelkiej oschłości i rozczarowań jest ona wieczna jak trawa. Przyjmuj pogodnie to co lata niosą, bez goryczy wyrzekając się przymiotów młodości. Rozwijaj siłę ducha by w nagłym nieszczęściu mogła być tarczą dla ciebie. Lecz nie dręcz się tworami wyobraźni. Wiele obaw rodzi się ze znużenia i samotności.\r\n\r\n     Jesteś dzieckiem wszechświata, nie mniej niż gwiazdy i drzewa, masz prawo być tutaj i czy jest to dla ciebie jasne czy nie, nie wątp, że wszechświat jest taki jaki być powinien.\r\n\r\n     Tak więc bądź w pokoju z Bogiem, cokolwiek myślisz i czymkolwiek się zajmujesz i jakiekolwiek są twe pragnienia: w zgiełku ulicznym, zamęcie życia, zachowaj pokój ze swą duszą. Z całym swym zakłamaniem, znojem i rozwianymi marzeniami ciągle jeszcze ten świat jest piękny. Bądź uważny, staraj się być szczęśliwy.\r\n\r\nMax Ehrmann",
                    Sentence = sentences[0]
                },

                new SentenceTranslation()
                {
                    Language = english,
                    Title = "Einstein bike",
                    Text = @"Life is like riding a bicycle. To keep your balance, you must keep moving.\r\n Albert Einstein",
                    Sentence = sentences[1]
                },
                new SentenceTranslation()
                {
                    Language = polish,
                    Title = "Rower einsteina",
                    Text = @"Życie jest jak jazda na rowerze.\r\n Żeby utrzymać równowagę musisz się poruszać naprzód.\r\n Albert Einstein",
                    Sentence = sentences[1]
                },

                new SentenceTranslation()
                {
                    Language = polish,
                    Title = "Dwa wilki",
                    Text = @"Pewien stary Indianin Cherokee nauczał swoje wnuki. Powiedział im tak:\r\n– Wewnątrz mnie odbywa się walka. To straszna walka.\r\n\r\n\r\nWalczą dwa wilki:\r\n\r\njeden reprezentuje strach, złość, zazdrość, smutek, żal, chciwość, arogancję, użalanie się nad sobą, poczucie winy, urazę, poczucie niższości, kłamstwa, fałszywą dumę i poczucie wyższości.\r\n\r\nDrugi to radość, zadowolenie, zgoda, pokój, miłość, nadzieja, akceptacja, chęć zrozumienia, hojność, prawda, życzliwość, współczucie i wiara.\r\n\r\nTaka sama walka odbywa się wewnątrz was i każdej innej osoby.\r\n\r\nDzieci myślały o tym przez chwilę, po czym jedno z nich zapytało:\r\n– Dziadku,  a który wilk wygra?\r\n– Ten, którego nakarmisz – odpowiedział stary Indianin.",
                    Sentence = sentences[2]
                },
                new SentenceTranslation()
                {
                    Language = english,
                    Title = "Two wolves",
                    Text = @"An old Cherokee is teaching his grandson about life. \r\n-It is a terrible fight and it is between two wolves. One is evil – he is anger, envy, sorrow, regret, greed, arrogance, self-pity, guilt, resentment, inferiority, lies, false pride, superiority, and ego.\r\n- He continued, \r\n-The other is good – he is joy, peace, love, hope, serenity, humility, kindness, benevolence, empathy, generosity, truth, compassion, and faith. The same fight is going on inside you – and inside every other person, too. The grandson thought about it for a minute and then asked his grandfather, \r\n-Which wolf will win? \r\n-The old Cherokee simply replied, \r\n-The one you feed.",
                    Sentence = sentences[2]
                },

                new SentenceTranslation()
                {
                    Language = polish,
                    Title = "Fortuna",
                    Text = @"Fortes fortuna adiuvat \r\n Szczęście sprzyja śmiałym",
                    Sentence = sentences[3]
                },
                new SentenceTranslation()
                {
                    Language = english,
                    Title = "Fortuna",
                    Text = @"Fortes fortuna adiuvat \r\n Fortune favors the bold",
                    Sentence = sentences[3]
                }
            };
            using (var context = new MotivatorModelContainer())
            {

                var recreateDbScript = File.ReadAllText(@"F:\C# Projects\TheMotivator\RestApi\Models\MotivatorModel.edmx.sql");
                Console.WriteLine(context.Database.SqlQuery(typeof(string), recreateDbScript));

                var testUser = new User()
                {
                    DeviceToken = "1",
                    Username = "TestUser",
                    Language = polish,
                    OwnedSentences = new List<Sentence>(),
                    LastVisitTime = DateTime.Now
                };
                testUser.OwnedSentences.Add(sentences[3]);
                testUser.OwnedSentences.Add(sentences[2]);

                #region addOrUpdateVersion
                //foreach (var lang in langs)
                //{
                //    context.Languages.AddOrUpdate(lang);
                //}
                //foreach (var sentence in sentences)
                //{
                //    context.Sentences.AddOrUpdate(sentence);
                //}
                //foreach (var translation in sentenceTranslations)
                //{
                //    context.SentenceTranslations.AddOrUpdate(translation);
                //}
                //foreach (var tag in tags)
                //{
                //    context.SentenceTags.AddOrUpdate(tag);
                //}
                //foreach (var tagTranslation in tagTranslations)
                //{
                //    context.SentenceTagTranslations.AddOrUpdate(tagTranslation);
                //}
                //context.Users.AddOrUpdate(testUser);
                #endregion
                
                context.Languages.AddRange(langs);
                context.Sentences.AddRange(sentences);
                context.SentenceTranslations.AddRange(sentenceTranslations);
                context.SentenceTags.AddRange(tags);
                context.SentenceTagTranslations.AddRange(tagTranslations);
                context.Users.Add(testUser);

                context.SaveChanges();

                Console.WriteLine("Changes in db saved");
                try
                {
                }
                catch (DbEntityValidationException e)
                {
                    var errors = "";
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        errors += "- Property: " + eve.Entry.Entity.GetType().Name + ", Error: " + eve.Entry.State + Environment.NewLine;
                        errors = eve.ValidationErrors.Aggregate(errors, (current, ve) => current + ("- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + Environment.NewLine));
                        return errors;
                    }
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
            return "Initiation done!";
        }
    }
}
