﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestApi.Models;

namespace RestApi.Controllers
{
    [RoutePrefix("api/users")]
    public class UserController : ApiController
    {
        [Route("new/{username}/{lang}")]
        [HttpGet]
        public string NewUser(string username, string lang)
        {
            return RegisterNewUser(username, lang);
        }

        private static string RegisterNewUser(string name = "", string lang = "english")
        {
            using (var context = new MotivatorModelContainer())
            {
                var newUser = new User()
                {
                    Username = string.IsNullOrEmpty(name) ? "Anonymous" : name,
                    DeviceToken = GenerateUniqueToken(context),
                    Language = context.Languages.FirstOrDefault(l => l.Name == lang),
                    LastVisitTime = DateTime.Now
                };

                context.Users.Add(newUser);
                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    var errors = "";
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        errors += "- Property: " + eve.Entry.Entity.GetType().Name + ", Error: " + eve.Entry.State + Environment.NewLine;
                        errors = eve.ValidationErrors.Aggregate(errors, (current, ve) => current + ("- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + Environment.NewLine));
                    }
                    return errors;
                }
                catch (Exception e)
                {
                    return e.Message;
                }
                return newUser.DeviceToken;
            }
        }

        private static string GenerateUniqueToken(MotivatorModelContainer context)
        {
            const int length = 14;
            var r = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            
            var token = "";
            do
            {
                token = new string(Enumerable.Repeat(chars, length)
                    .Select(s => s[r.Next(s.Length)]).ToArray());
            } while (context.Users.Any(user => user.DeviceToken == token));

            return token;
        }
    }
}
                   