﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestApi.Helpers;
using RestApi.Models;
using RestApi.ModelsOfTransactions;

namespace RestApi.Controllers
{
    [RoutePrefix("api/data")]
    public class DataModelController : ApiController
    {
        [Route("bytoken/{token}")]
        public ClientData GetCompleteDataModelForUserWithToken(string token)
        {
            using (var context = new MotivatorModelContainer())
            {
                User user;
                DAL.ReciveUserFromToken(token, context, out user);

                //test
                var testList = context.Users.Where(u => u.DeviceToken == token).ToList();

                if (user.IsNull())
                    throw new HttpResponseException(HttpStatusCode.BadRequest);

                return user.ConstructClientDataObject(context);
            }
        }
    }
}
