﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using RestApi.Helpers;
using RestApi.Models;
using RestApi.ModelsOfTransactions;

namespace RestApi.Controllers
{
    [RoutePrefix("api/sentences")]
    public class SentencesController : ApiController
    {
        [Route("actual/bytoken/{token}")]
        public SerializableSentence GetActualSentence(string token)
        {
            var actualSentence = DAL.ConstructActualSentenceFromToken(token);
            if (actualSentence.IsNull())
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            return actualSentence;
        }

        [Route("owned/bytoken/{token}")]
        public List<SerializableSentence> GetAllSentencesForUserWithToken(string token)
        {
            var ownedSentences = DAL.ConstructListOfAllSentencesFromToken(token);
            if (ownedSentences.IsNull())
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            return ownedSentences;
        }
    }
}
