using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using RestApi.Models;
using RestApi.ModelsOfTransactions;

namespace RestApi.Helpers
{
    public static class DAL
    {
        public static SerializableSentence ConstructActualSentenceFromToken(string token)
        {
            using (var context = new MotivatorModelContainer())
            {
                User user;
                if (!ReciveUserFromToken(token, context, out user))
                    return null;

                return user.GetActualSentece(context);
            }
        }

        public static List<SerializableSentence> ConstructListOfAllSentencesFromToken(string token)
        {
            using (var context = new MotivatorModelContainer())
            {
                User user;
                return !ReciveUserFromToken(token, context, out user) ? null : user.GetAllOwnedSentences(context);
            }
        }

        /// <summary>
        /// Receive user data for given token
        /// </summary>
        /// <param name="token">Device token for user</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static bool ReciveUserFromToken(string token, MotivatorModelContainer context, out User user)
        {
            var usersWithToken = context.Users.Where(u => u.DeviceToken == token) as DbQuery<User>;
            if (usersWithToken.IsNull() || usersWithToken.Count() != 1)
            {
                user = null;
                return false;
            }
            user = usersWithToken.FirstOrDefault();
            return true;
        }
    }
}