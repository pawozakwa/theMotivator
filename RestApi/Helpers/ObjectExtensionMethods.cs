namespace RestApi.Helpers
{
    public static class ObjectExtensionMethods
    {
        public static bool IsNull(this object o)
        {
            return o == null;
        }
    }
}