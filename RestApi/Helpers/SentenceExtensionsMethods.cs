using System.Collections.Generic;
using System.Linq;
using RestApi.Models;
using RestApi.ModelsOfTransactions;

namespace RestApi.Helpers
{
    public static class SentenceExtensionsMethods
    {
        public static SerializableSentence GetSerializableSentence(this Sentence sentence, Language lang, MotivatorModelContainer dbContext)
        {
            var sentenceTranslation =
                dbContext.SentenceTranslations
                    .Where(st => st.SentenceSentenceId == sentence.SentenceId)
                    .FirstOrDefault(st => st.Language.LanguageId == lang.LanguageId);
            
            //var translatedTagsEntityList =
            //    dbContext.SentenceTagTranslations.Where(sts =>
            //        sentence.SentenceTags.Contains(sts.SentenceTag)).Where(sts => 
            //        sts.Language == lang).ToList();

            var tagsList = new List<SentenceTag>();
            var translationsList = new List<SentenceTagTranslation>();
            foreach (var tag in sentence.SentenceTags)
            {
                tagsList.Add(tag);
                translationsList.AddRange(tag.SentenceTagTranslations.Where(translation => translation.Language == lang));
            }
            
            var constructedSentece = new SerializableSentence(sentence, sentenceTranslation, tagsList, translationsList);
            
            return constructedSentece;
        }
    }
}