﻿using System;
using System.Collections.Generic;
using System.Linq;
using RestApi.Models;
using RestApi.ModelsOfTransactions;

namespace RestApi.Helpers
{
    public static class UserExtensionsMethods
    {
        public delegate Sentence ChooseNewActualSentenceStrategy(List<Sentence> notOwnedSentences);

        public static SerializableSentence GetActualSentece(this User user, MotivatorModelContainer dbContext)
        {
            if (user.ActualSentence.IsNull())
            {
                user.GiveNewActualSentence(dbContext, ChooseRandomActualSentence);
                dbContext.SaveChanges();
            }
            var actualSentence = user.ActualSentence;
            return actualSentence.GetSerializableSentence(user.Language, dbContext);
        }

        public static List<SerializableSentence> GetAllOwnedSentences(this User user, MotivatorModelContainer dbContext)
        {
            return user.OwnedSentences.Select(sentence => sentence.GetSerializableSentence(user.Language, dbContext)).ToList();
        }

        public static void GiveNewActualSentence(this User user, MotivatorModelContainer dbContext, ChooseNewActualSentenceStrategy chooseNewActualSentenceStrategystrategy)
        {
            var userAsQuery = dbContext.Users.Single(u => u.UserId == user.UserId);
            var actualSentece = userAsQuery.ActualSentence;
            var allSentences = dbContext.Sentences.ToList();
            var notOwnedSentencesList = allSentences.Except(user.OwnedSentences).ToList();
            notOwnedSentencesList.Remove(actualSentece);
            user.ActualSentence = chooseNewActualSentenceStrategystrategy(notOwnedSentencesList);
        }

        public static Sentence ChooseRandomActualSentence(List<Sentence> notOwnedSentences)
        {
            return notOwnedSentences.OrderBy(_ => Guid.NewGuid()).Take(1).First();
        }

        public static ClientData ConstructClientDataObject(this User user, MotivatorModelContainer dbContext)
        {
            return new ClientData(user, dbContext);
        }
    }
}